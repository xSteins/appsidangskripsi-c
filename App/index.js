import express from 'express';
import path from 'path';
import session from 'cookie-session';
import crypto from 'crypto';
import cookieParser from 'cookie-parser';

const app = express();
app.use(cookieParser());
const port = 8005;
const publicPath = path.resolve('static-path');

app.use(express.static(publicPath));
app.set('view engine', 'ejs');

import bodyParser from 'body-parser';
app.use(bodyParser.urlencoded({ extended: true }));

import mysql from 'mysql';
// MySQL Connection
const pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'dbreviewtas',
    "typeCast": function castField(field, useDefaultTypeCasting) {
        if ((field.type === "BIT") && (field.length === 1)) {
            var bytes = field.buffer();
            return (bytes[0]);
        }
        return (useDefaultTypeCasting());
    }
});

// Middleware connection
const key1 = crypto.randomBytes(32).toString('hex');
const key2 = crypto.randomBytes(32).toString('hex');
app.use(
    session({
        name: 'session',
        keys: [key1, key2],
        secret: 'randomizedstringforvalue',
        resave: false,
        saveUninitialized: true,
        cookie: {
            secure: true,
            httpOnly: true,
            maxAge: 16000,
        },
    })
);

app.listen(port, () => {
    console.log('App started');
    console.log(`Server running on http://localhost:${port}`);
});

// auth middleware for role
const auth = (req, res, next) => {
    // tidak ada role = belum terdaftar
    if (!req.user || !req.user.role) {
        return res.redirect('/login');
        // return res.status(403).json({ error: 'Unauthorized' });
    }

    const role = req.user.role;

    if (req.path === '/admin-menu' && role !== 'admin') {
        // obsfucate admin page.
        return res.redirect('/404', ({ errorMsg: 'Halaman tidak Ditemukan' })); // Redirect to 404 if not admin
    }

    if (req.path === '/menu-dosen' && (role === 'admin' || role === 'mahasiswa')) {
        // blokir apabila role != dosen
        return res.redirect('/404', ({ errorMsg: 'Halaman hanya untuk dosen' }));
    }

    if (req.path === '/menu-mahasiswa' && role !== 'mahasiswa') {
        // blokir apabila role != mahasiswa
        return res.redirect('/404', ({ errorMsg: 'Halaman hanya untuk mahasiswa' }));
    }

    next();
}

app.get('/login', async (req, res) => {
    res.render('login', { errorMsg: null, success: null });
})

app.post('/login', (req, res) => {
    const email = req.body.email;
    const password = req.body.password;
    const accountQuery =
        'SELECT `E_mail`, `Password` FROM `account` WHERE `E_mail` = ? AND `Password` = ?';
    // 'SELECT `Id_Account`,`E_mail`, `Password` FROM `account` WHERE `E_mail` = ? AND `Password` = ?';
    const accountParams = [email, password];

    pool.query(accountQuery, accountParams, (error, results) => {
        if (error) {
            console.log(error);
        } else if (results.length > 0) {
            // const user = results[0];
            // res.cookie('Id_Account', user.Id_Account)
            // res.cookie('email', user.E_mail);
            // res.cookie('role', user.);

            // if (user.Account_Role === 'admin') {
            //     res.redirect('/admin-menu');
            // }
            // else if (user.Account_Role === 'dosen') {
            //     res.redirect('/menu-dosen')
            // }
            // else {
            //     res.redirect('/');
            // }
            // res.render('/home');
            console.log("Successfully validated");
            res.redirect('/home');
        } else {
            res.render('login', {
                errorMsg: 'Password / email anda salah.',
                success: false,
            });
        }
    });
});

app.get('/', async (req, res) => {
    res.redirect('/layout');
    // res.render('layout')
})

app.get('/layout', async (req, res) => {
    res.render('layout');
})

// todo : implementing auth to check role
// app.get('/home', auth, async (req, res) => {
app.get('/home', async (req, res) => {
    res.render('home');
})


app.get('/manajemen-sidang', auth, async (req, res) => {
    res.render('dashboard-sidang');
})
